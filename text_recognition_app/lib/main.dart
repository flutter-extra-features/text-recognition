import 'package:flutter/material.dart';

import 'package:text_recognition_app/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}
/*
  Packages Used :-
    1] flutter pub add gallery_picker
    2] flutter pub add google_mlkit_text_recognition

  Changes done in  build.gradle for google mlkittextrecogintion

  minSdkVersion: 21
  targetSdkVersion: 33
  compileSdkVersion: 34

  Changes done in  AndroidManifest.xml for gallery picker

  <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
  <uses-permission android:name="android.permission.READ_MEDIA_IMAGES"/>
   <uses-permission android:name="android.permission.READ_MEDIA_VIDEO"/>
*/